# spdm
Routines for the analysis of symmetric, positive-definite matrices. For details and instructions, see the [wiki](https://gitlab.com/fmriToolkit/spdm/wikis/home).
